<?php
include "connection.php";

$stmt = $con->prepare("INSERT INTO persons (firstname, lastname) VALUES (?, ?)");
$stmt->bind_param("ss", $firstname, $lastname);

$firstname = "John";
$lastname = "Doe";
$stmt->execute();

$firstname = "yram";
$lastname = "eom";
$stmt->execute();

$firstname = "iluj";
$lastname = "oley";
$stmt->execute();

echo "New records created successfully";

$stmt->close();
?>