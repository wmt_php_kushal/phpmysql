<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link real="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<title>Books</title>
<?php
include "welcome.php";?>
</head>
<body class="bg-dark">
    <?php
    $auth_id="";
        include "connection.php";
        // $sql="Select * from books";
        $sql="Select books.id, books.title ,books.pages,books.language,authors.fullname, books.coverimage, books.isbn_no, books.description, books.status from books,authors where authors.id=bookauthor";
        $result= $con->query($sql);
        echo "<table class='table table-striped table-dark'>";
        echo "<tr><th>title</th>";
        echo "<th>pages</th>";
        echo "<th>language</th>";
        echo "<th>bookauthor</th>";
        echo "<th>coverimage</th>";
        echo "<th>isbn_no</th>";
        echo "<th>description</th>";
        echo "<th>status</th><th>Update</th><th>delete</th><th></th><th></th></tr>";
        if($result->num_rows>0){
        while($row=$result->fetch_assoc()){
            // $auth_id=$row['bookauthor'];
            // $sqlid="select * from authors where id=$auth_id";
            // $res= $con->query($sqlid);
            // if($res->num_rows>0){
            // while($row=$res->fetch_assoc()){
            //     $authname=$row['firstname'];
            
            // }
            // }else{
            //     echo "0 results";
            // }
            $sesid=$row["id"];
            
            echo "<tr><td>" .$row["title"]. "</td>";
            echo "<td>". $row["pages"]."</td>";
            echo "<td>" . $row['language']."</td>";
            echo "<td>" . $row['fullname']."</td>";
            echo "<td>" . $row['coverimage']."</td>";
            echo "<td>" . $row['isbn_no']."</td>";
            echo "<td>" . $row['description']."</td>";
            echo "<td>" . $row['status']."</td>";
            $ustatus=$row['status'];
            echo "<td><a class='btn btn-info' name='insert' href='updatebook.php?id=$sesid'>Update</a></td>";
            echo "<td><a class='btn btn-danger' name='insert' href='deletebook.php?id=$sesid'>Delete</a></td>";
            echo "<td><a class='btn btn-success' name='insert' href='showbook.php?id=$sesid'>Show Books</a></td>";
            if($ustatus==0){

                echo "<td><a class='btn btn-success' name='insert' href='activeinactive.php?id=$sesid'>Active</a></td></tr>";
            }else{
                echo "<td><a class='btn btn-danger' name='insert' href='activeinactive.php?id=$sesid'>Inactive</a></td></tr>";
            }
        }
                echo "</table>";
            }else{
                echo "0 results";
            }
    ?>
    <a class="btn btn-primary" name="insert" href="bookinsert.php">INSERT</a>
</body>
</html>