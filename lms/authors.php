<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link real="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <title>Authors</title>
</head>
<body class="bg-dark">

    
    
    
    <?php
        include "connection.php";
        include "welcome.php";
        $sql="Select * from authors";
        $result= $con->query($sql);
        if($result->num_rows>0){
            echo "<table class='table table-striped table-dark'>";
            echo "<tr><th>Fullname</th>";
            echo "<th>DOB</th>";
            echo "<th>Gender</th>";
            echo "<th>Address</th>";
            echo "<th>Phone</th>";
            echo "<th>Description</th>";
            echo "<th>Status</th><th>Update</th><th>delete</th></tr>";
            
            while($row=$result->fetch_assoc()){
                $sesid=$row["id"];
                echo "<tr><td>" .$row["fullname"]. "</td>";
                echo "<td>" . $row['dob']."</td>";
                echo "<td>" . $row['gender']."</td>";
                echo "<td>" . $row['address']."</td>";
                echo "<td>" . $row['phone']."</td>";
                echo "<td>" . $row['description']."</td>";
                echo "<td>" . $row['status']."</td>";
                echo "<td><a class='btn btn-primary' name='insert' href='updateauthor.php?id=$sesid'>Update</a></td>";
                echo "<td><a class='btn btn-primary' name='delete' href='deleteauthor.php?id=$sesid'>Delete</a></td></tr>";
            }
            echo "</table>";
        }else{
            echo "0 results";
        }
    ?>
    <a class="btn btn-primary" name="insert" href="authorinsert.php">INSERT</a>
</body>
</html>