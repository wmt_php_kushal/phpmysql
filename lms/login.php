<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link real="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script>

    <script>
      $(document).ready(function() {
        var e_mail = $("#email").val();
        var pass = $("#pass").val();
        $("#passerror").text("");
        $('#emailerror').text("");
        
        $("#email").focusout(function() {
            e_mail = $("#email").val();
            var re = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            var is_email = re.test(e_mail);
            if (e_mail == "" || e_mail == null) {
                $('#emailerror').text("Requeired Field email");
            }
            else{
                if(!is_email){
                    console.log("invalid");
                    $('#emailerror').text("Invalid email");
                }else{
                    $('#emailerror').text("");
                }
            }
        });

        $("#pass").focusout(function() {
            pass = $("#pass").val();
            
            var is_pass=/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(pass);
            if (pass == "" || pass == null) {
            $("#passerror").text("Please enter passsword");
            }
            else{
              if(!is_pass)
                {
                    $("#passerror").text("Please enter Proper passsword");
                    console.log(is_pass);
                    console.log(rep);
                }else
                {
                    $("#passerror").text("");
                }
            } 
          
        });
        
        $("#frm").submit(function(){
            
            e_mail = $("#email").val();
            pass = $("#pass").val();
            if(e_mail =="" || pass =="")
            {
             alert("please fill the the Credentaials");          
              return false;
            }
        })
        console.log(e_mail);
      });
    </script>
    <title>Login to LMS</title>
</head>
<body>

    <div class="d-flex justify-content-center">
        <h1 class="display-4">Login to LMS</h1>
    </div>
    <div class="sf  d-flex justify-content-center m-0 p-0">
        <br>
        <div class= "d-flex justify-content-center mt-5">
            <form method="post" action="?" id="frm">
            <?php
include 'connection.php';


$emailErr=$passerr="";
function test_input($data)
{
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
if(isset($_POST['btnlogin']))
{   

    $err="";
    $email=$pass="";
    $email=$_POST['email'];
    $pass=$_POST['pass'];

    
    
    if (empty($email)) {
        $err=TRUE;
        $emailErr = "Email is required";
        //echo "<script>alert('Email is required');</script>";
      } else {
        $emailchk = test_input($_POST["email"]);
        // check if e-mail address is well-formed
        if (!filter_var($emailchk, FILTER_VALIDATE_EMAIL)) {
            $err=TRUE;
            $emailErr='Invalid email format';
        }else{
            $err=FALSE;
            $emailErr='';
        }
      }
      if(empty($pass)){
          $err=TRUE;
          
          $passerr="Password required";
      }
      else{
        if (!preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/i',$pass))
        {
          $err=TRUE;  
          $passerr='Password is Invalid';
              
        }
        else
        {
          $err=FALSE;
          $passerr='';
        }
           

      }
      if($err==FALSE){
        $sql="select * from login where email='".$email. "' and password='".$pass."';";
        $result=$con->query($sql);
        if($result->num_rows>0){
            header('Location:welcome.php');
            }
        else{
            echo "<script>alert('ERROR');</script>";
        }
        
    }
}
?>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelpId" placeholder="">
                    <p id="emailerror" class="text-danger"></p>
                    <span><?php echo $emailErr;?>
                </div>
                <div class="form-group">
                    <label for="Password">Password</label>
                    <input type="password" class="form-control" name="pass" id="pass" placeholder="">
                    <p id="passerror" class="text-danger"></p>
                    <?php echo $passerr;?>
                </div>
                <button type="submit" class="btn btn-primary justify-content-center" name="btnlogin">Login</button>
            </form>
        </div>
    </div>
</body>
</html>
