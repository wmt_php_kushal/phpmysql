
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link real="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script>
    $(document).ready(function() {
    $('#updateform').validate({
      rules:{
        title: {
          required : true
        },
        pages:{
          required: true
        },
        lang:{
          required: true,

        },
        bookauthor:{
          required: true
        },
        ci:{
          required: true
        },
        isbn:{
          required: true
        },
        desc:{
          required: true
        }
    }
    })
  });
    </script>
    <title>Update Books</title>
</head>
<body>
<?php
     $sesid=$_GET["id"];
    
    include "connection.php";
    include "welcome.php";
    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    $title=$pages=$desc=$lang=$bookauthor=$ci=$isbn=$status="";
    $utitle=$upages=$udesc=$ulang=$ubookauthor=$uci=$uisbn=$ustatus="";
    // +++++++++++++++++++++++++
    $sql="Select * from books where id=$sesid";
    $result= $con->query($sql);
    if($result->num_rows>0)
    {        
        while($row=$result->fetch_assoc()){
            $utitle=$row['title'];
            $upages= $row["pages"];
            $ulang = $row['language'];
            $ubookauthor = $row['bookauthor'];
            $uci= $row['coverimage'];
            $uisbn =$row['isbn_no'];
            $udesc =$row['description'];
            $ustatus= $row['status'];
            
        }
        echo "</table>";
    }else{
        echo "0 results";
    }
    //++++++++++++++++++++++++++ 

    if(isset($_POST['btnupdate'])){
        
        {
            echo "<script>alert(' Updated records ');</script>";
            header('location:books.php');
        }
    }
    
?>
<div class="container">
        <form method="post" action=""id="updateform">
            <div class="form-group">
              <label for="">Title</label>
              <?php echo " - " .$utitle;?>
            </div>
            <div class="form-group">
              <label for="">Pages</label>
              <?php echo " - " .$upages;?>
            </div>
            <div class="form-group">
              <label for="">lang</label>
              <?php echo " - ". $ulang;?>
            </div>
            
            <div class="form-group">
                <label for="">bookauthor </label>
                
                    <?php
                    include "connection.php";
                        $sql="Select * from authors where id=$ubookauthor"; 
                        $result= $con->query($sql);
                        if($result->num_rows>0){
                            while($row=$result->fetch_assoc()){
                                $sesid=$row["id"];
                                echo " - " . $row["firstname"];
                                }
                        }
                    ?>
            </div>
            <div class="form-group">
              <label for="">Cover Image</label>
              <?php echo " - " .$uci;?>
            </div>
            <div class="form-group">
              <label for="">isbn</label>
              <?php echo " - " .$uisbn;?>
            </div>
            <div class="form-group">
              <label for="">Description</label>
              <?php echo " - " .$udesc;?>
            </div>
            <div class="form-group">
              <label for="">status</label>
              <?php echo " - " .$ustatus;?>
            </div>
            <button type="submit" class="btn btn-primary"name="btnupdate">Update</button>
        </from>
    </div>
    
</body>
</html>