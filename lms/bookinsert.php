<!DOCTYPE html>
<?php include 'welcome.php';?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>

    <title>Insert into BOOKS</title>
    <style>
    .error{
      color:red;
    }
    </style>
    
</head>
<body>
<script>
    $(document).ready(function() {
      $.validator.addMethod( "isbn_number", function( isbn_number, element ) {
        isbn_number = isbn_number.replace( /\(|\)|\s+|-/g, "" );
        return this.optional( element ) || isbn_number.length > 9 &&
          isbn_number.match( /^(?:ISBN(?:-10)?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$)[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$/ );
      }, "Please Provide a valid ISBN number" );
    $('#insbform').validate({
      rules:{
        title : {
          required : true
        },
        pages : {
          required: true
        },
        lang : {
          required : true,

        },
        bookauthor : {
          required: true
        },
        cv: {
          required : true
        },
        isbn :{
          required: true,
          isbn_number : true
        },
        desc : {
          required : true
        }
    }
    })
  });
    </script>
<?php
function test_input($data)
{
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
    // include "welcome.php";
    include "connection.php";
    $title=$pages=$desc=$lang=$bookauthor=$cv=$isbn="";
    $count=0;
    $titleErr=$pagesErr=$descErr=$langErr=$bookauthorErr=$cvErr=$isbnErr="";
    if(isset($_POST['btninsert'])){
        $title = $_POST['title'];
        $pages = $_POST['pages'];
        $lang = $_POST['lang'];
        $bookauthor = $_POST['bookauthor'];
        $cv = $_POST['cv'];
        $isbn = $_POST['isbn'];
        $desc = $_POST['desc'];
        // $status = $_POST['status'];
        //
        if (empty($title)) {
          $errtitle=TRUE;
          $count--;
          $titleErr='Title is required';
        }
        else{
              $errtitle=FALSE;
              $count++;
              $titleErr='';
        }
          if (empty($pages)) {
            $errpages=TRUE;
            $count--;
            $pagesErr='pages name is required';
          }
          else{
            $errpages=FALSE;
            $count++;
            $pagesErr='';
          }
          if (empty($isbn)) {
            $errisbn=TRUE;
            $count--;
            $isbnErr='ISBN is required';
          }
          else{
            if (!preg_match('/^(?:\d[\ |-]?){9}[\d|X]$/i',$isbn))
            {
              $errisbn=TRUE;
              $count--;
              $isbnErr='ISBN number is Invalid';
            }
            else
            {
              $errisbn=FALSE;
              $count++;
              $isbnErr='';
            }
          }
          if (empty($lang)) {
            $errlang=TRUE;
            $count--;
            $langErr='Language is required';          
          }  
          else{
            if(!preg_match('/[a-zA-z]/',$lang)){
              $errlang=TRUE;
              $count--;
              $langErr='Language should not contain anything Other than aplhabets';
            }else{
              $errlang=FALSE;
              $count++;
              $langErr='';
            }
          }
          if (empty($bookauthor)|| $bookauthor=="Select bookauthor") {
            $errbookauthor=TRUE;
            $count--;
            $bookauthorErr='Please Select bookauthor';
          }  
          else{
                $errbookauthor=FALSE;
                $count++;
                $bookauthorErr="";
          }
          if (empty($ci)) {
            $errci=TRUE;
            $count--;
             $ciErr='cover image is required';
          }
          else{
            $errci=FALSE;
            $count++;
            $ciErr='';
          }
          
          if (empty($desc)) {
            $errdesc=TRUE;
            $count--;
            $descErr='Description is required';
          }
          else{
                $errdesc=FALSE;
                $count++;
                $descErr='';
          }
        //
        if($err==FALSE){
            
          $ins="insert into books (title,pages,language,bookauthor,coverimage,isbn_no,description) values ('$title','$pages','$lang','$bookauthor','$cv','$isbn','$desc')";
          $ins_result = $con->query($ins);
          if($ins_result==FALSE){
              // echo "<script>alert('Erorr inserting records SOme Fields are Left to FILL');</script>";
          }
          else
          {
              echo "<script>alert(' inserting records ');</script>";
              echo '<script>window.location="books.php"</script>';
          }
        }else{
          echo "<script>alert('Erorr inserting records SOme Fields are Left to FILL');</script>";
        }
    }
    ?>
<div class="container">
        <form method="post" action="?" id="insbform">
            <div class="form-group">
              <label for="">Title</label><span>*</span>
              <input type="text" class="form-control" name="title" id="title" aria-describedby="helpId" placeholder="">
              <span class='error'><?php echo $titleErr?></span>
            </div>
            <div class="form-group">
              <label for="">Pages</label><span>*<span>
              <input type="text" class="form-control" name="pages" id="pages" aria-describedby="helpId" placeholder="" min=1>
              <span class='error'><?php echo $pagesErr;?></span>            
            </div>
            <div class="form-group">
              <label for="">Language</label><span>*<span>
              <input type="text" class="form-control" name="lang" id="lang" aria-describedby="helpId" placeholder="">
              <span class='error'><?php echo $langErr;?></span>
            </div>
            
            <div class="form-group">
                <label for="">Book Author</label><span>*<span>
                <select class="custom-select" name="bookauthor" id="bookauthor">
                 <option hidden>Select bookauthor</option>
                    <?php
                    include "connection.php";
                        $sql="Select * from authors"; 
                        $result= $con->query($sql);
                        if($result->num_rows>0){
                            while($row=$result->fetch_assoc()){
                                $sesid=$row["id"];
                                echo "<option value=". $row["id"].">". $row["fullname"]."</option>";
                                }
                        }
                    ?>
                </select>
                <span class='error'><?php echo $bookauthorErr;?></span>            
            </div>
            <div class="form-group">
              <label for="">Cover Image</label><span>*<span>
              <input type="text" class="form-control" name="cv" id="cv" aria-describedby="helpId" placeholder="">
              <span class='error'><?php echo $cvErr;?></span>            
            </div>
            <div class="form-group">
              <label for="">ISBN No.</label><span>*<span>
              <input type="text" class="form-control" name="isbn" id="isbn" aria-describedby="helpId" placeholder="">
              <span class='error'><?php echo $isbnErr;?></span>
            </div>
            <div class="form-group">
              <label for="">Description</label>
              <textarea class="form-control" name="desc" id="desc" rows="3"></textarea>
              <span class='error'><?php echo $descErr;?></span>
            </div>
            <button type="submit" class="btn btn-primary"name="btninsert">Insert</button>
        </from>
    </div>
    
</body>
</html>