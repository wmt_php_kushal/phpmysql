<!DOCTYPE html>
<?php include 'welcome.php';?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link real="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <title>Insert into Author</title>
    
    <style>
      .error{
        color:red;
      }
    </style>

</head>
<body>
<script>
    $(document).ready(function() {
          $.validator.addMethod( "mobile", function( phone_number, element ) {
          phone_number = phone_number.replace( /\(|\)|\s+|-/g, "" );
          return this.optional( element ) || phone_number.length > 9 &&
            phone_number.match( /^[0-9]{10}+$/ );
          }, "Please specify a valid mobile number" );
    $('#insform').validate({
      rules:{
        fname: {
          required : true
        },
        lname:{
          required: true
        },
        dob:{
          required: true,
          date: true
        },
        gender:{
          required: true
        },
        address:{
          required: true
        },
        phone:{
          required: true
          
        },
        desc:{
          required: true
        }
    }

    })
  });
    </script>
    <?php
    // include "welcome.php";
    include "connection.php";
    function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
    $fname=$lname=$desc=$dob=$gender=$address=$phone=$count="";
    $fnameErr=$lnameErr=$descErr=$dobErr=$genderErr=$addressErr=$phoneErr=$err="";
    $errfname=$errdob=$errgender=$erraddress=$errphone=$errdesc=$errstatus="";
    if(isset($_POST['btninsert'])){
      // if($_SERVER["REQUEST_METHOD"] == "POST"){
        $fname = test_input($_POST['fname']);
        $dob = test_input($_POST['dob']);
        $gender = test_input($_POST['gender']);
        $address = test_input($_POST['address']);
        $phone = test_input($_POST['phone']);
        $desc = test_input($_POST['desc']);
        $count=0;
        // $status = $_POST['status'];
        //
        if (empty($fname)) {
          echo $err;
          $count--;
          $fnameErr='FullName is required';
          $errfname=TRUE;
        }
        else{
          if(!preg_match('/^[A-Za-z][A-Za-z]*(?: [A-Za-z]+)*$/', $fname)){
            echo $err;
            $errfname=TRUE;
            $count--;
            $fnameErr='Full name should only start with ALPHABETIC character NO OTHE SPECIAL CHARACTERS ALLOWED';
        }else{
          $errfname=FALSE;
          $count++;
          $fnameErr='';
        }
      }
        
        if (empty($dob)|| $dob=="dd/mm/yyyy") {
          $errdob=TRUE;
          $count--;
          $dobErr='Date of Birth is required';          
        }  
        else{
              $errdob=FALSE;
              $count++;
              $dobErr='';
        }
        if (empty($gender)|| $gender=="Select Gender") {
          $errgender=TRUE;
          $count--;
          $genderErr='Please Select Gender';
        }  
        else{
          $errgender=FALSE;
          $count++;
          $genderErr="";
        }
        if (empty($address)) {
          $erraddress=TRUE;
          $count--;
          $addressErr='Address is required';
        }
        else{
          $erraddress=FALSE;
          $count++;
          $addressErr='';
        }
        if (empty($phone)) {
          $errphone=TRUE;
          $count--;
          $phoneErr='phone number is required';
        }
        else{
          if(!preg_match('/^[0-9]{10}+$/', $phone)){
            $errphone=TRUE;
            $phoneErr="Please Enter the valid moblie number";
          }else{
            $errphone=FALSE;
            $count++;
            $phoneErr='';
          }
        }
        if (empty($desc)) {
            $errdesc=TRUE;
            $count--;
            $descErr='Description is required';
        }
        else{
            $errdesc=FALSE;
            $count++;
            $descErr='';
        }
        
        
        // if($err==FALSE){
          if($errfname==FALSE && $errdob == FALSE && $errgender==FALSE && $erraddress==FALSE && $errphone==FALSE && $errdesc==FALSE){
          // if($count==7){
            
            $ins="insert into authors (fullname,dob,gender,address,phone,description) values ('$fname','$dob','$gender','$address',$phone,'$desc')";
            $ins_result = $con->query($ins);
            if($ins_result==FALSE){
              echo "<script>alert('Erorr inserting records');</script>";
          }
          else
          {
              echo "<script>alert(' inserting records ');</script>";
              header('location:authors.php');
          }
          }else{
            echo "<script>alert(' Error inserting records ');</script>";
          }
        //
        
        
    }
    ?>
    
<div class="container sf">
        <form method="post" action="" id="insform">
            <div class="form-group">
              <label for="">Firstname</label><span>*</span>
              <input type="text" class="form-control" name="fname" id="fname" aria-describedby="helpId" placeholder="">
              <span><?php echo $fnameErr;?></span>
            </div>
            <div class="form-group">
              <label for="">DOB</label><span>*<span>
              <input type="date" class="form-control" name="dob" id="dob" aria-describedby="helpId" placeholder="">
              <span><?php echo $dobErr ?></span>

            </div>
            
            <div class="form-group">
                <label for="">Gender</label><span>*<span>
                <select class="custom-select" name="gender" id="gender">
                    <option hidden>Select Gender</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                </select>
                <span><?php echo $genderErr ?></span>

            </div>
            <div class="form-group">
              <label for="">Address</label><span>*<span>
              <textarea class="form-control" name="address" id="address" rows="3"></textarea>
              <span><?php echo $addressErr ?></span>
            </div>
            <div class="form-group">
              <label for="">Phone</label><span>*<span>
              <input type="number" class="form-control" name="phone" id="phone" aria-describedby="helpId" placeholder="">
              <span><?php echo $phoneErr ?></span>
            </div>
            <div class="form-group">
              <label for="">Description</label>
              <textarea class="form-control" name="desc" id="desc" rows="3"></textarea>
              <span><?php echo $descErr ?></span>
            </div>
            <button type="submit" class="btn btn-primary"name="btninsert">Insert</button>
        </from>
    </div>
    
</body>
</html>