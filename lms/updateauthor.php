
<!DOCTYPE html><?php include 'welcome.php';?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link real="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    
    <script>
    $(document).ready(function() {
    $('#updateform').validate({
      rules:{
        fname: {
          required : true
        },
        lname:{
          required: true
        },
        dob:{
          required: true,
          date: true
        },
        gender:{
          required: true
        },
        address:{
          required: true
        },
        phone:{
          required: true
        },
        desc:{
          required: true
        }
    }

    })
  });
    </script>
    <title>Update Author</title>
</head>
<body>
<?php
// include "welcome.php";
     $sesid=$_GET["id"];
    
    include "connection.php";
    function test_input($data)
{
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
    $fname=$lname=$desc=$dob=$gender=$address=$phone=$status="";
    $fnameErr=$lnameErr=$descErr=$dobErr=$genderErr=$addressErr=$phoneErr="";
    $fname=$ulname=$udesc=$udob=$ugender=$uaddress=$uphone=$ustatus="";
    $errfname=$errdob=$errgender=$erraddress=$errphone=$errdesc=$errstatus="";
// +++++++++++++++++++++++++
$sql="Select * from authors where id=$sesid";
        $result= $con->query($sql);
        if($result->num_rows>0){
           
            
            while($row=$result->fetch_assoc()){

                $ufname=$row["fullname"];
                $udob = $row['dob'];
                $ugender = $row['gender'];
                $uaddress= $row['address'];
                $uphone =$row['phone'];
                $udesc =$row['description'];
                $ustatus= $row['status'];
                
            }
            echo "</table>";
        }else{
            echo "0 results";
        }
//++++++++++++++++++++++++++ 

        // if(isset($_POST['btnupdate'])){
        if($_SERVER["REQUEST_METHOD"] == "POST"){
          
    $fnameErr=$lnameErr=$descErr=$dobErr=$genderErr=$addressErr=$phoneErr="";
    // $ufname=$ulname=$udesc=$udob=$ugender=$uaddress=$uphone=$ustatus="";
    $err="";
        $fname = test_input($_POST['fname']);
        $dob = test_input($_POST['dob']);
        $gender = test_input($_POST['gender']);
        $address = test_input($_POST['address']);
        $phone = test_input($_POST['phone']);
        $desc = test_input($_POST['desc']);
        $status=test_input($_POST['status']);
        $count=0;
        if (empty($fname)) {
          echo $err;
          $count--;
          $fnameErr='FullName is required';
          $errfname=TRUE;
        }
        else{
          if(!preg_match('/^[A-Za-z][A-Za-z]*(?: [A-Za-z]+)*$/', $fname)){
            echo $err;
            $errfname=TRUE;
            $count--;
            $fnameErr='Full name should only start with ALPHABETIC character NO OTHE SPECIAL CHARACTERS ALLOWED';
        }else{
          $errfname=FALSE;
          $count++;
          $fnameErr='';
        }
      }
        
        if (empty($dob)|| $dob=="dd/mm/yyyy") {
          $errdob=TRUE;
          $count--;
          $dobErr='Date of Birth is required';          
        }  
        else{
              $errdob=FALSE;
              $count++;
              $dobErr='';
        }
        if (empty($gender)|| $gender=="Select Gender") {
          $errgender=TRUE;
          $count--;
          $genderErr='Please Select Gender';
        }  
        else{
              $errgender=FALSE;
              $count++;
              $genderErr="";
        }
        if (empty($address)) {
          $erraddress=TRUE;
          $count--;
          $addressErr='Address is required';
        }
        else{
              $erraddress=FALSE;
              $count++;
              $addressErr='';
        }
        if (empty($phone)) {
          $errphone=TRUE;
          $count--;
          $phoneErr='phone number is required';
        }
        else{
          if(!preg_match('/^[0-9]{10}+$/', $phone)){
            $errphone=TRUE;
            $phoneErr="Please Enter the valid moblie number";
          }else{
            $errphone=FALSE;
            $count++;
            $phoneErr='';
          }
        }
        if (empty($desc)) {
          $errdesc=TRUE;
          $count--;
          $descErr='Description is required';
        }
        else{
              $errdesc=FALSE;
              $count++;
              $descErr='';
        }
        if(empty($status)){
          $errstatus=TRUE;
          $count--;
          $statusErr="Status Field Cannot be empty";
        }else{
          $err=FALSE;
          $count++;
          $errstatus=FALSE;
          $statusErr="";
        }
        
        // if($err==FALSE){
          if($errfname==FALSE && $errdob == FALSE && $errgender==FALSE && $erraddress==FALSE && $errphone==FALSE && $errdesc==FALSE && $errstatus==FALSE){
          // if($count==7){
          $ins="update authors set fullname='$fname',dob='$dob',gender='$gender',address='$address',phone='$phone',description='$desc',status='$status' where id=$sesid";
          $ins_result = $con->query($ins);
          if($ins_result==FALSE){
              echo "<script>alert('Erorr Updating records ".$con->error."');</script>";
          }
          else
          {
              echo "<script>alert(' Updated records ');</script>";
              header('location:authors.php');
          }
        }else{
        }
        
    }
    
    ?>
<div class="container sf">
        <form method="post" action=""id="updateform">
            <div class="form-group">
              <label for="">Fullname</label>
              <input type="text" class="form-control" name="fname" id="fname" value="<?php echo $ufname?>" aria-describedby="helpId" placeholder="">
              <span><?php echo $fnameErr;?></span>
            </div>
            
            <div class="form-group">
              <label for="">DOB</label>
              <input type="date" class="form-control" name="dob" id="dob" value="<?php echo $udob?>" aria-describedby="helpId" placeholder="">
              <span><?php echo $dobErr ?></span>
            </div>
            
            <div class="form-group">
                <label for="">Gender</label>
                <select class="custom-select" name="gender" id="gender" >
                    <option hidden="">gender</option>
                    <option selected><?php echo $ugender?></option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
                <span><?php echo $genderErr ?></span>
            </div>
            <div class="form-group">
              <label for="">Address</label>
              <textarea class="form-control" name="address" id="address" rows="3"><?php echo $uaddress?></textarea>
              <span><?php echo $addressErr ?></span>
            </div>
            <div class="form-group">
              <label for="">Phone</label>
              <input type="number" class="form-control" name="phone" id="phone" value="<?php echo $uphone?>" aria-describedby="helpId" placeholder="">
              <span><?php echo $phoneErr ?></span>
            </div>
            <div class="form-group">
              <label for="">Description</label>
              <input type="text" class="form-control" name="desc" id="desc" value="<?php echo $udesc?>" aria-describedby="helpId" placeholder="" >
              <span><?php echo $descErr ?></span>
            </div>
            <div class="form-group">
              <label for="">status</label>
              <input type="int" class="form-control" name="status" id="status" value="<?php echo $ustatus?>" aria-describedby="helpId" placeholder="">
            </div>
            <button type="submit" class="btn btn-primary"name="btnupdate">Update</button>
        </from>
    </div>
    
</body>
</html>